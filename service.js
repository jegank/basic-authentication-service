const express = require("express");
const app = express();
const bodyparser = require('body-parser');
const port = process.env.PORT || 3200;
const Pool = require('pg').Pool
const connection = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'poc',
  password: 'postgres',
  port: 5432,
});


const ShutdownHook = require('shutdown-hook');
let shutdownHook = new ShutdownHook();
shutdownHook.register();
shutdownHook.on('ShutdownStarted', (e) => console.log('it has began'))
shutdownHook.on('ComponentShutdown', (e) => console.log('shutting down one component'))
shutdownHook.on('ShutdownEnded', (e) => {
  saveConfig();
  console.log('it has ended')
});

app.listen(port, () => {
  console.log(`running at port ${port}`);
});

// middleware
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));
app.use(function (req, res, next) {
  var allowedOrigins = ['http://localhost:3200','http://localhost:8080', 'http://localhost:3000'];
  var origin = req.headers.origin;
  if (allowedOrigins.indexOf(origin) > -1) {
    res.setHeader('Access-Control-Allow-Origin', origin);
  }
  res.header('Access-Control-Allow-Methods', 'GET, OPTIONS, POST, PUT');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  res.header('Access-Control-Allow-Credentials', true);
  return next();
});



app.post("/auth", (req, res) => {
		if (req.body) {
		   let {userName, password} = req.body;
		   let errorMessage = [];
		    if(userName == undefined) {
	          errorMessage.push("please enter the userName");
	        }
	        if(password == undefined) {
	         errorMessage.push("please enter the password");
	        }
			if(typeof errorMessage !== 'undefined' && errorMessage.length > 0) {
			    return res.status(422).send(errorMessage);
			}
		   
 		  let sql = `SELECT user_id as userId FROM account where username='${userName}' and password='${password}'`;	
           connection.query(sql, function(err, result, fields){
			   let data = result.rows;
			 if(typeof data !== 'undefined' && data.length > 0) {
		     res.status(200).send("success");
			 }else{
				 console.log("testing");
			  res.status(422).send(["you are not registered user. please sign up"]);
			 }
		  });
		}
		return res;
});

app.post("/register", (req, res) => {
	if(req.body) {
	 let errorMessage = [];
	 let {userName, password, email,firstName, lastName} = req.body;
	   
	 if(userName == undefined) {
	   errorMessage.push("please enter the userName");
	 }
	 if(password == undefined) {
	   errorMessage.push("please enter the password");
	 }
	 if(email == undefined) {
	   errorMessage.push("please enter the email");
	 }
	 if(firstName == undefined) {
	   errorMessage.push("please enter the firstName");
	 }
	 if(lastName == undefined) {
	   errorMessage.push("please enter the lastName");
	 }
	 if(typeof errorMessage !== 'undefined' && errorMessage.length > 0) {
			    return res.status(422).send(errorMessage);
	 }
	 resolveAfter1Seconds(userName, email).then(()=>{
	 	 let sql = `insert into account(username,password,email,first_name,last_name)values('${userName}','${password}','${email}','${firstName}','${lastName}')`;
		 connection.query(sql, function (err, result, fields) {
			if (err) throw err;
			res.status(200).send({response : "inserted"});
		  });
	 }).catch(error=> {
	     res.status(422).send(["username or email are already. Existing please give different one"]);
	 })
	}
	return res;
});

function resolveAfter1Seconds(username, email) {
  return new Promise((resolve,reject) => {
    setTimeout(function() {
		  let sql = `SELECT user_id as userId FROM account where username='${username}' or email='${email}'`;
          connection.query(sql, function(err, result, fields) {
		  if (err) throw err;
		  let data = result.rows;
		  if(typeof data !== 'undefined' && data.length > 0) {
		     reject(false);
		  }else {
		     resolve(true);
		  }
		 })	
    }, 1000)
  })
}

app.get("/health", (req, res) => (res.status(200).json("Helloworld")));








